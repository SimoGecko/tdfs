﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public static class Utility {
	// --------------------- VARIABLES ---------------------
	
	// public


	// private


	// references
	
	
	// --------------------- EXTENSTION METHODS ------------------
	public static Vector3 To3(this Vector2 v) {
        return new Vector3(v.x, v.y, 0);
    }
    public static Vector2 Evaluate(this Rect r, Vector2 v) {
        return new Vector2( r.xMin + v.x * r.width, r.yMin + v.y * r.height);
    }
    public static Vector2 Evaluate(this Rect r) {
        return r.Evaluate(new Vector2(Random.value, Random.value));
    }

    public static Vector2 RandomV2() {
        return new Vector2(Random.value * 2 - 1, Random.value * 2 - 1);
    }
	
	
	// --------------------- CUSTOM METHODS ----------------
	
	
	// commands



	// queries



	// other
	
}