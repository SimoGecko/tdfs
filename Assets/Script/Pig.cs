﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class Pig : Unit {
    // --------------------- VARIABLES ---------------------

    // public


    // private


    // references


    // --------------------- BASE METHODS ------------------
    protected override void Start() {
        base.Start();

    }

    protected override void Update() {
        base.Update();

    }



    // --------------------- CUSTOM METHODS ----------------


    // commands



    // queries



    // other

}