﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class Projectile : MonoBehaviour {
    // --------------------- VARIABLES ---------------------

    // public
    public int damage = 10;
    public float speed = 1f;
    public Dir dir;
    public int team;

	// private


	// references
	
	
	// --------------------- BASE METHODS ------------------
	void Start () {
		
	}
	
	void Update () {
        transform.position += ((dir == Dir.R) ? Vector3.right : Vector3.left) * speed * Time.deltaTime;

    }



    // --------------------- CUSTOM METHODS ----------------


    // commands
    //private void OnCollisionEnter2D(Collision2D collision) {
    //    Debug.Log("proj collided");
    //    IDamageable idam = collision.gameObject.GetComponent<IDamageable>();
    //    if (idam != null) {
    //        idam.TakeDamage(damage);
    //        Destroy(gameObject);
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision) {
        IDamageable idam = collision.gameObject.GetComponent<IDamageable>();
        if (idam != null && team!=idam.Team()) {
            idam.TakeDamage(damage);
            Destroy(gameObject);
        }
    }

    public void Initialize(int dam, float sp, Dir d, int t) {
        damage = dam; speed = sp; dir = d; team = t;
    }



    // queries



    // other

}