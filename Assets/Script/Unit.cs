﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public enum Dir { L, R};
public enum UnitType { Chicken, Sheep, Pig, Cow, Farm};
public delegate void Action();

public class Unit : LivingEntity {
    // --------------------- VARIABLES ---------------------

    // public
    public UnitType type;
    public int cost;
    public float speed;
    [HideInInspector]
    public Dir dir = Dir.R;
    public float timeBetweenActions;
    public Action callback;

    // private


    // references


    // --------------------- BASE METHODS ------------------
    protected virtual void Start () {
        StartCoroutine(CallbackAction());

    }

    protected virtual void Update () {
        Move();
	}



    // --------------------- CUSTOM METHODS ----------------


    // commands
    void Move() {
            transform.position += ((dir== Dir.R)?Vector3.right:Vector3.left) * speed * Time.deltaTime;
    }


    // queries



    // other
    IEnumerator CallbackAction() {
        while (!dead) {
            float waitTime = Random.Range(.8f, 1.2f) * timeBetweenActions;
            yield return new WaitForSeconds(waitTime);
            if(callback!=null) callback();
        }
    }

}