﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class AI : Team {
    // --------------------- VARIABLES ---------------------

    // public
    public float EggsPerMinute = 30;
    public Vector2 spawnSize;
    // private


    // references
    public Unit[] units;
	
	
	// --------------------- BASE METHODS ------------------
	protected override void Start () {
        base.Start();
        StartCoroutine(IncreaseBalance());
        StartCoroutine(SpawnEnemy());
    }

    protected override void Update () {
        base.Update();
	}



    // --------------------- CUSTOM METHODS ----------------


    // commands
    void TrySpawnEnemy() {
        Unit randomUnit = units[Random.Range(0, units.Length)];
        if (CanAfford(randomUnit.cost)) {
            Unit newUnit = Instantiate(randomUnit, transform.position + Vector2.Scale(Utility.RandomV2(), spawnSize).To3(), Quaternion.identity);
            newUnit.dir = Dir.L;
            newUnit.team = 1;
            Spend(randomUnit.cost);
        }
    }


    // queries
    IEnumerator SpawnEnemy() {
        while (true) {
            yield return new WaitForSeconds(1f);
            TrySpawnEnemy();
        }
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube(transform.position, spawnSize);
    }


    // other
    IEnumerator IncreaseBalance() {
        while (true) {
            yield return new WaitForSeconds(60f / EggsPerMinute);
            Earn(1);
        }
    }
	
}