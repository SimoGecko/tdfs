﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class PlacerM : MonoBehaviour {
    // --------------------- VARIABLES ---------------------

    // public
    public Unit[] unitPrefabs;
    Dictionary<string, Unit> unitDic;
    const float placeDelay = .2f;

    // private
    bool currentlyPlacing = false;
    Unit currentUnit;
    Unit ghostUnit;
    float startPlacingTime;


	// references
	
	
	// --------------------- BASE METHODS ------------------
	void Start () {
        CreateUnitDictionary();
	}
	
	void Update () {
        if(currentlyPlacing && Time.time > startPlacingTime + placeDelay) {
            //move ghost unit
            ghostUnit.transform.position = PlacePos();

            if (Input.GetMouseButtonDown(1))
                StopPlacing();
            else if (Input.GetMouseButtonDown(0)) {
                Place(PlacePos());
            }
        }
        

        
	}

	
	
	// --------------------- CUSTOM METHODS ----------------
	
	
	// commands
    void CreateUnitDictionary() {
        unitDic = new Dictionary<string, Unit>();
        foreach (Unit u in unitPrefabs)
            unitDic.Add(u.type.ToString(), u);
    }

    public void TryPlacing(string unit) {
        if (!unitDic.ContainsKey(unit)) {
            Debug.LogError("unit " + unit + " doesn't exist");
            return;
        }
        Unit unitToPlace = unitDic[unit];
        if (Player.instance.CanAfford(unitToPlace.cost)) {
            StartPlacing(unitToPlace);
        } else {
            Debug.Log("Cannot afford!");
        }
    }

    void StartPlacing(Unit unit) {
        currentlyPlacing = true;
        currentUnit = unit;
        startPlacingTime = Time.time;
        //show ghost unit
        ghostUnit = Instantiate(currentUnit, PlacePos(), Quaternion.identity) as Unit;
        ghostUnit.enabled = false;
        ghostUnit.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, .5f);
    }

    void StopPlacing() {
        Destroy(ghostUnit.gameObject);
        ghostUnit = null;
        currentlyPlacing = false;
        currentUnit = null;
    }

    void Place(Vector2 pos) {
        Unit newUnit = Instantiate(currentUnit, pos, Quaternion.identity) as Unit;
        newUnit.team = 0;
        newUnit.dir = Dir.R;
        Player.instance.Spend(currentUnit.cost);
        StopPlacing();
    }



	// queries
    Vector2 PlacePos() {
        return Camera.main.ScreenPointToRay(Input.mousePosition).origin;
    }



	// other
	
}