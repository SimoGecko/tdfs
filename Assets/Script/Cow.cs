﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class Cow : Unit {
    // --------------------- VARIABLES ---------------------

    // public
    public int damage;
    public float projSpeed;


    // private


    // references
    public Projectile proj;


    // --------------------- BASE METHODS ------------------
    protected override void Start() {
        base.Start();
        callback = Shoot;

    }

    protected override void Update() {
        base.Update();

    }



    // --------------------- CUSTOM METHODS ----------------


    // commands
    void Shoot() {
        Projectile newProj = Instantiate(proj, transform.position, Quaternion.identity);
        newProj.Initialize(damage, projSpeed, dir, team);
    }


    // queries



    // other

}