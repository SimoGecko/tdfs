﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public interface IDamageable {
    void TakeDamage(int damage);
    int Team();
}

////////// DESCRIPTION //////////

public class LivingEntity : MonoBehaviour, IDamageable {


    // --------------------- VARIABLES ---------------------

    // public
    public int startingHealth = 100;


    // private
    protected int health;
    protected bool dead;
    [HideInInspector]
    public int team;


    // references


    // --------------------- BASE METHODS ------------------
    void Start () {
        health = startingHealth;
	}
	
	void Update () {
		
	}



    // --------------------- CUSTOM METHODS ----------------


    // commands
    public void TakeDamage(int damage) {
        health -= damage;
        if (health <= 0 && !dead) Die();
    }

    void Die() {
        dead = true;
       //Destroy(gameObject);
    }

    public int Team() {
        return team;
    }

    // queries



    // other

}