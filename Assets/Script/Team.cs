﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


////////// DESCRIPTION //////////

public class Team : MonoBehaviour {
    // --------------------- VARIABLES ---------------------

    // public
    public int startCredits = 10;

    // private
    [HideInInspector]
    public int numCredits;


    // references


    // --------------------- BASE METHODS ------------------

    protected virtual void Start() {
        numCredits = startCredits;
    }

    protected virtual void Update() {

    }



    // --------------------- CUSTOM METHODS ----------------


    // commands
    public void Earn(int i) {
        numCredits += i;
    }
    public void Spend(int i) {
        numCredits = Mathf.Max(0, numCredits - i);
    }
    

    // queries
    public bool CanAfford(int cost) {
        return numCredits >= cost;
    }



    // other

}