﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class Egg : MonoBehaviour {
    // --------------------- VARIABLES ---------------------

    // public
    const int valuePerEgg = 1;

	// private


	// references
	
	
	// --------------------- BASE METHODS ------------------
	void Start () {
		
	}
	
	void Update () {
		
	}


    void OnMouseDown() {
        Player.instance.Earn(valuePerEgg);
        Destroy(gameObject);
    }



    // --------------------- CUSTOM METHODS ----------------


    // commands



    // queries



    // other

}