﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


////////// DESCRIPTION //////////

public class Player : Team {
    // --------------------- VARIABLES ---------------------

    // public

    // private


    // references
    public Text creditsText;
    public static Player instance;


    // --------------------- BASE METHODS ------------------
    private void Awake() {
        instance = this;
    }

    protected override void Start () {
        base.Start();
	}
	
	protected override void Update () {
        base.Update();
        UpdateCreditsUI();
	}

	
	
	// --------------------- CUSTOM METHODS ----------------
	
	
	// commands
    void UpdateCreditsUI() {
        if(creditsText!=null) creditsText.text = numCredits.ToString();
    }


    // queries


	// other
	
}