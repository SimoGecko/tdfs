﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class Farm : Unit {
    // --------------------- VARIABLES ---------------------

    // public
    public Rect eggSpawnArea;
    public bool generateEggs = false;

	// private


	// references
    public Egg eggPrefab;


    // --------------------- BASE METHODS ------------------
    protected override void Start () {
        base.Start();
        if(generateEggs) callback = SpawnOneEgg;
	}

    protected override void Update () {
        base.Update();
	}



    // --------------------- CUSTOM METHODS ----------------


    // commands
    void SpawnOneEgg() {
        Debug.Log("farm called");
        Vector3 spawnPos = transform.position + eggSpawnArea.Evaluate().To3();
        Egg newEgg = Instantiate(eggPrefab, spawnPos, Quaternion.identity) as Egg;
    }

    void MakeRect() {
        
    }

    // queries
    

    // other

}