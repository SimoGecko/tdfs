﻿// (c) Simone Guggiari 2017

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


////////// DESCRIPTION //////////

public class Chicken : Unit {
    // --------------------- VARIABLES ---------------------

    // public
    public float eggSpawnRadius = 2f;


    // private


    // references
    public Egg eggPrefab;


    // --------------------- BASE METHODS ------------------
    protected override void Start() {
        base.Start();

        callback = SpawnOneEgg;
    }

    protected override void Update() {
        base.Update();
    }



    // --------------------- CUSTOM METHODS ----------------


    // commands
    void SpawnOneEgg() {
        Vector3 spawnPos = transform.position + (Random.insideUnitCircle * eggSpawnRadius).To3();
        Egg newEgg = Instantiate(eggPrefab, spawnPos, Quaternion.identity) as Egg;
    }



    // queries



    // other

}